"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_grpc urls *

:details: lara_grpc urls module.
         - add app specific urls here

:authors: 

:date: (creation)          

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.urls import path, include
from django.views.generic import TemplateView

# Add your lara_grpc urls here.

urlpatterns = [
    # path('', TemplateView.as_view(template_name='index.html'), name='lara_grpc-main-view'),
    # path('subdir/', include('.urls')),
]
