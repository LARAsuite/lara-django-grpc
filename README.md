# lara_grpc - A LARA python-django app.


## Installation of lara_grpc


1.  Install lara-django

    see Installation at [lara-django]()[https://gitlab.com/LARAsuite/lara-django)

2.  Install the app

        cd [repository of lara_grpc]
        pip3 install .

3.  Add "lara_grpc" to your INSTALLED_APPS setting like this:

        INSTALLED_APPS = [
            ...
            'lara_grpc',
        ]

4.  Include the lara-django-app URLconf in your project urls.py like
    this:

        path('app/', include('lara_grpc.urls')),

5.  Run python manage.py migrate to create the lara_grpc models.
6.  In case you like to test the app, please load the demo data:
7.  Start the development server and visit
    <http://127.0.0.1:8000/admin/> to create a device (you'll need the
    Admin app enabled).

## Environment variables

for development, please set : 

        export DJANGO\_SETTINGS\_MODULE=lara.settings.devel

for production, please set : 

        export DJANGO\_SETTINGS\_MODULE=lara.settings.production

if your media does not reside in the default media folder, please set
environment variable to :

         export DJANGO\_MEDIA\_PATH='path/to/my/media'

to use user defined fixtures, please set: : 
export DJANGO\_FIXTURE\_PATH='path/to/user/fixtures'

## Installation of required packages

        pip3 install --user requirements/devel.py

## Testing all applications

use this command to run all tests:

        python3 manage.py test

### tox

### testing the application

from the directory where the main lara project is located, type

        python3 manage.py test lara_grpc

## Generating documentation

To generate the documentation, please change directory to app and run:

        sphinx-apidoc -o docs . 
        make html

## Acknowledgements

The LARA-django developers thank

    -   the python team
    -   the whole [django team](<https://www.djangoproject.com/) for their great framework !

