"""_____________________________________________________________________

:PROJECT: LARA

*lara_grpc setup *

:details: lara_grpc setup file for installation.
         - For installation, run:
           run pip3 install .
           or  python3 setup.py install

:file:    setup.py
:authors: 

:date: (creation)          
:date: (last modification) 

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

import os
import sys

from setuptools import setup, find_packages
#~ from distutils.sysconfig import get_python_lib

pkg_name = 'lara_grpc'

def read(fname):
    try:
        return open(os.path.join(os.path.dirname(__file__),"..","docs", fname)).read()
    except IOError:
        return ''

install_requires = [] 
data_files = []
package_data = {}  #{pkg_name: ['lara_grpc/templates/*.html', 'lara_grpc/static/css/*.css',  ]}
    
setup(name=pkg_name,
    version=__version__,
    description='lara_grpc',
    long_description=read('README.rst'),
    author='',
    author_email='',
    keywords='lab automation, projects, experiments, database, evaluation, visualisation, SiLA2, robots',
    url='https://github.com/LARAsuite/lara_grpc',
    license='MIT',
    packages=find_packages(), #['lara_grpc'],
    #~ package_dir={'lara_grpc':'lara_grpc'},
    install_requires = install_requires,
    test_suite='',
    classifiers=['Development Status :: 1 - Development',
                   'Environment :: commandline',
                   'Framework :: django',
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: MIT',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   'Programming Language :: Python :: 3.7',
                   'Programming Language :: Python :: 3.8',
                   'Topic :: Utilities'],
    include_package_data=True,
    #~ package_data = package_data,
    data_files=data_files,
)
